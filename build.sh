#!/bin/bash

###
# 'build.sh'
# This script will build the program 
#	intended to be run from
# 'ProgramName'/working/src/
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 05-09-2019--17:03:36
###

###
# Error Messages
###

WRONG_DIRECTORY="Move To Source Directory To Build Program"
YES_OR_NO="Invalid please type [Y]es or [N]o"
ZA="../../.executable/zaRefreshDate.sh"
ZB="../../.executable/zbBackupProgram.sh"
ZC="../../.executable/zcCopyProgram.sh"
ZD="../../.executable/zdCompile.sh"
SH_LOG="../../iofiles/sh.log"
X_ERR="../../iofiles/x.err"

if [[ ! $PWD/ = */working/src/ ]]; then
  cd working/src
	if [[ ! $PWD/ = */working/src/ ]]; then
		echo $WRONG_DIRECTORY
		exit 1
  fi
fi

#  running every time slows computation
read -p "Refresh Date...? [Y]es / [N]o:    " yn_
case "$yn_" in
  [yY][eE][sS]|Y|y ) # yes
    echo "Executing zaRefreshDate.sh" > $SH_LOG
    ./$ZA >> $SH_LOG
    ;;
  [nN][oO]|N|n ) # no
    echo "Skipping zaRefreshDate.sh" > $SH_LOG
    ;;
  * ) # else
    echo $YES_OR_NO
    ;;
esac
echo "-----------------------------------------------------------------" >> $SH_LOG
echo "Executing zbBackupProgram.sh" >> $SH_LOG
echo "-----------------------------------------------------------------" >> $SH_LOG
./$ZB >> $SH_LOG
echo "Executing zcCopyProgram.sh" >> $SH_LOG
echo "-----------------------------------------------------------------" >> $SH_LOG
./$ZC >> $SH_LOG
echo "Executing zdCompile.sh" >> $SH_LOG
echo "-----------------------------------------------------------------" >> $SH_LOG
./$ZD |& tee $X_ERR

echo "Program Done Building" 

###
# End 'build.sh'
###