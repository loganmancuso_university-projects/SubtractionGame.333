#!/bin/bash
###
# 'zbBackupProgram.sh'
# this program will copy all files in 'working'
# and will tar the file as a backup to the backup
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 11-29-2017--11:04:36
###

cd ../../
echo "Remove the old 'backup' directory"
rm -r .backup/
echo "Create a new 'backup' directory"
mkdir .backup/
#
echo "Descend into 'working' directory"
cd working/
tar -czf project.tar.gz *
mv project.tar.gz ../.backup/project.tar.gz

###
# End 'zbBackupProgram.sh'
###