/**
 * 'main.c'
 * Main file for programming in c
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 06-05-2019--12:35:51
 * 
 * This Program Simulates a Game of .333, similar to that of 
 * Dr.Nim board game. It takes input from the xin.txt file 
 * for the number of heaps you want to play with. It will then
 * populate the heaps with a random number of blocks between 
 * [min_ and max_]. It will print the Game Board and proceed to 
 * ask the user for input. The computer will calculate the 
 * best move and the game loops until the heaps are empty. 
**/

/**
 * included files/library
**/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

/**
 * global variables
**/

//heap max_ is 20 blocks, at most 10 heaps
int* heap_ = 0; //heap array
int i,j,k = 0; //loop variables
int max_,min_; //cannot be set to zero or it will reset value constantly 
int number_of_heaps_,turn_,done_,game_value_,wins_,losses_ = 0; //variables 

/**
 * Function: 'GameOver'
 * This function will calculate if the game is over by counting
 * the number of empty heaps, and comparing to the total number
 * of heaps in this game.
**/
int GameOver() {
  int is_zero = 0;
  //count number of piles that have zero blocks
  for (i = 0; i < number_of_heaps_; i++) {
    //all of the heaps need to be zero to end the game
    if (heap_[i] != 0) { 
      return 0;
    } else {
    //  printf("This Heap Is Empty %d\n",i);
      is_zero++; //game over 
    } //end else
  }//end for loop i
  if (is_zero == number_of_heaps_) {
   // printf("is_zero:%d == number_of_heaps_ %d\n",is_zero,number_of_heaps_);
    return 1;
  }//end if zero is number
  return 0;
} //end GameOver

/**
 * Function: 'FindGameValue'
 * This function will calculate the total game value of the 
 * heaps by using XOR
**/
void FindGameValue() {
  game_value_ = heap_[0]%4;
  for (i = 1; i < number_of_heaps_; i++) {
    if (heap_[i] != 0) {
      int temp = heap_[i]%4;
      game_value_ ^= temp;
    } //end if
  } //end for loop
} //end FindGameValue 

/**
 * Function: 'PrintHeap'
 * This function will print a heap with X's as blocks
 * the array heap_ is a global array with values to represent 
 * the number of blocks left in the heap. The X's are not stored
 * in the heap_ array rather the for loop only runs the length
 * of the number of blocks in each pile
**/
void PrintHeap() {
  printf("[Value\t|Heap\t]\n");
  for (i = 0; i < number_of_heaps_; i++) {
    int value = heap_[i]%4;
    //print header info for the heap (heap size) | heap number 
    printf("[*%d\t|%d\t]\t",value,i);
    for (j = 0; j < heap_[i]; j++) {
      printf(" X"); //print X's as blocks
    } //end for loop j
    printf("\n");
  }//end for loop i 
  printf("Total Game Value: *%d\n",game_value_);
} //end PrintHeap

/**
 * Function: 'RemoveBlock'
 * This function will remove blocks from the heap
 * It will take in 2 integers, one is the heap number and the 
 * other number is the quantity to remove from the heap
 * it will then calculate if the game is over based on the 
 * number of empty heaps in the array heap_
**/
void RemoveBlock(int heap_number, int to_remove) {
  heap_[heap_number] = (heap_[heap_number] - to_remove);
  FindGameValue(); //recalculate 
} //end RemoveBock

/**
 * Function: 'ComputerTurn'
 * This function will mimic the computers move to choose the best
 * possible move for the turn. 
**/
void ComputerTurn() {
  printf("Computer Calculating Move\n");
  int to_remove = -1;
  int heap_number = -1;
  int test_heap[number_of_heaps_];
  for (i = 0; i < number_of_heaps_; i++) {
    test_heap[i] = heap_[i];
  } //end loop to build test heap
  for (i = 0; i < number_of_heaps_; i++) {
  //  printf("Checking Heap @ %d\n",i);
    if (test_heap[i] != 0) {
      for (j = 1; j <= 3; j++) {
        if (test_heap[i] >= j) {
        //  printf("Removing %d\n",j);
          test_heap[i] = (test_heap[i] - j);
          int temp_game_val = test_heap[0]%4;
          for (k = 1; k < number_of_heaps_; k++) {
            if (test_heap[k] != 0) {
              int temp = test_heap[k]%4;
            //  printf("%d ^= %d\n",temp_game_val,test_heap[k]);
              temp_game_val ^= temp;
            //  printf("= %d\n",temp_game_val);
            } //end if test heap k is zero
          } //end k loop
        //  printf("Calculated Game Value %d\n",temp_game_val);
          if (temp_game_val == 0) {
            to_remove = j;
            heap_number = i;
        //    printf("Found Zero Position Heap %d Number %d\n",to_remove,heap_number);
            turn_++;
            printf("Computer Removing %d From Heap # %d\n",to_remove, heap_number);
            RemoveBlock(heap_number,to_remove);
            return;
          } //end temp game value == 0
        } //end if test heap >= j
        test_heap[i] = (test_heap[i] + j);
      } // end j loop (removing 1, 2, or 3 blocks)
    } //end if test heap != 0
  } // end i loop number of heaps
  if (to_remove == -1 && heap_number == -1) {
    printf("Zero Position Not Found\n");
    do {
      heap_number = (rand() % (number_of_heaps_ + 0 - 0)) + 0;  
  //    printf("Heap_Number randomly generated %d\n", heap_number);
    } while (heap_[heap_number] == 0); //end while
  //  printf("Found Non Empty Heap Making Move\n");
    if (heap_[heap_number] >= 3) { //heap is greater than 3
      to_remove = (rand() % (3 + 1 - 1)) + 1; //rand 
    } else if (heap_[heap_number] < 3 && heap_[heap_number] > 1) { //heap is 2
      to_remove = (rand() % (2 + 0 - 0)) + 0; //rand 
    } else { //heap is 1
      to_remove = 1; 
    } //end else 
    turn_++;
    printf("Computer Removing %d From Heap # %d\n",to_remove, heap_number);
    RemoveBlock(heap_number,to_remove);
  } //end end if == -1
} //end ComputerTurn

/**
 * Function: 'UserTurn'
 * This function will take the user input of two integer values
 * The first is the heap number and the other is the quantity
 * to remove from that heap. The function will check that the 
 * integers are a valid move and if so it will call 
 * RemoveBlock and pass the values to the function
 * if the bounds fail in any way the function will re-prompt the
 * user for a new guess
**/
void UserTurn() { 
  int to_remove = 0;
  int heap_number = 0; 
  int try = 0;
  do { 
    printf("Enter Heap To Remove From \n");
    fflush(stdout);
    scanf("%d", &heap_number);
    printf("Enter Quantity To Remove \n");
    fflush(stdout);
    fflush(stdout); 
    scanf("%d", &to_remove);
    printf("User Removing %d From Heap # %d\n",to_remove, heap_number);
    //check heap number 
    if ((heap_number > number_of_heaps_)
    || (heap_number < 0)) {
      //invalid heap number 
      printf("Invalid Heap Pile Try Again\n");
      fflush(stdout);
    } else if ((to_remove > heap_[heap_number])
    || (to_remove < 1)
    || (to_remove > 3)) {
      printf("Invalid Quantity To Remove Try Again\n");
      fflush(stdout);
    } else {
      try = 1;
    } //end else
  } while (try == 0); //end do-while
  fflush(stdout);
  turn_++;
  //re-do heap
  RemoveBlock(heap_number, to_remove);
} //end UserTurn

/**
 * Function: 'Game'
 * This function is the single game of NIM loop
**/
void Game() {
  //populate with random numbers
  //srand ( time(NULL) );
  for (i = 0; i < number_of_heaps_; i++) {
    heap_[i] = (rand() % (max_ + 1 - min_)) + min_;
  } //end i loop
  FindGameValue(); //calculate game value initially
  //Game Starting Here
  printf("Game Of .333\nRemove 1, 2, or 3 Blocks, But Don't Get Stuck!\n");
  PrintHeap();
  fflush(stdout); // Will now print everything in the stdout buffer
  //Start Game Loop
  do {
    //UserTurn();
    ComputerTurn(); //computer will play itself
    fflush(stdout); 
    PrintHeap();
    fflush(stdout);
    if (GameOver() == 1) {
      break;
    } //end if
    ComputerTurn();
    fflush(stdout); 
    PrintHeap();
    fflush(stdout);
  } while (GameOver() != 1); //end single game loop 
  //print who won the game
  if (turn_%2 != 0) {
    printf("First Player Wins\n");
    wins_++;
  } //end if
  else {
    printf("Second Player Wins\n");
    losses_++;
  } //end else 
  printf("Game Terminated\n");
} //end Game

/**
 * Function: 'Main'
 * This function will simulate a Game of Subtraction .333 
 * with valid move being to remove 1, 2, or 3 blocks from 
 * each pile.
**/

int main(int argc, char *argv[]) {

  /**
   * x.in = argv 1
   * x.out = argv 2
   * x.log = argv 3
  **/

  char const* const in_file = argv[1];
  char const* const out_file = argv[2];
  char const* const log_file = argv[3];
  FILE *file = fopen(in_file, "r");
  printf("Reading From File %s\n",in_file);
  char line[256];
  int int_arr[3];
  int i = 0;
  while(fgets(line, sizeof(line), file)) {
    printf("Line Read: %d\n",atoi(line));
    int_arr[i] = atoi(line);
    i++;
  }

  printf("Beginning Computation\n");

  /**
    * add your code here
  **/
  number_of_heaps_ = int_arr[0];
  min_ = int_arr[1];
  max_ = int_arr[2];

/*
  printf("Before\n");
  printf("number_of_heaps_ %d \n",number_of_heaps_);
  printf("min %d, max %d \n",min_,max_);
*/
  //if min_ or max_ variables are wrong
  srand ( time(NULL) ); //random generation comment out for seeded generation 
  if (min_ <= 0 || max_ <= 0 || min_ >= max_) {
    min_ = 1;
    max_ = (rand() % (25 + 2 - 2)) + 2;
  }
  //if number of heaps is less than or equal to zero randomly generate a heap count
  if (number_of_heaps_ <= 0) {
    number_of_heaps_ = (rand() % (max_ + 1 - min_)) + min_; 
  } //end if
/*
  printf("After\n");
  printf("number_of_heaps_ %d \n",number_of_heaps_);
  printf("min %d, max %d \n",min_,max_);
*/

  heap_ = malloc(sizeof(int) * number_of_heaps_); 
  int again_ = 0;

    //uncomment out to check cpu time 
    clock_t t;
    t = clock();
    //Game();
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    printf("function took %f seconds to execute \n", time_taken);


  
  do {
    Game();
    printf("Play Again? 1 = Yes 0 = No \n");
    fflush(stdout); 
    scanf("%d", &again_);
    turn_ = 0; //set the turn back to zero to count the amount taken 
  } while (again_ != 0); //end multi game loop
  
  printf("Tournament Complete:\nWins:\t%d Losses:\t%d\n",wins_,losses_);


  printf("Main Function Complete\n");

  return 0;

}//end main

/**
 * End 'main.c'
**/